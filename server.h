#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <pthread.h>
#include <unistd.h>
#include "client.h"

int isValidPosition(int id, int x, int y);
void broadcast(char* msg, struct node *clients);
void *handleClient(void *client);
