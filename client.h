struct client{
	int clientId;
	int xPos;
	int yPos;
	int socketDesc;
};

struct node{
	struct client *c;
	struct node *nextClient;
};

void insertClient(int id, int socket);
void removeClient(int id);
void updatePos(int id, int x, int y);
