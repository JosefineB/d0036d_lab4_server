#include "client.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

struct node *clients = NULL;
struct node *currentClient = NULL;

void insertClient(int id, int socket){
	struct node *newC = malloc(sizeof(struct node));
	struct client *nC = malloc(sizeof(struct client));
	//newC->c = (struct client*) malloc(sizeof(struct client));
	newC->c = nC;
	newC->c->clientId = id;
	newC->c->socketDesc = socket;
	//Link client together with the rest
	newC->nextClient = clients;
	//update head to new client
	clients = newC; 
}

void removeClient(int id){
	if(clients = NULL){
		return;
	}
	struct node *temp = clients;
	while(temp != NULL){
		if((temp->c->clientId) == id){ //If the client to delete is first
			clients = temp->nextClient;
			free(temp);
			return;
		}
		else if((temp->nextClient->c->clientId) == id){ //Search for client in the list
			struct node *removeNode = temp->nextClient;
			temp->nextClient = temp->nextClient->nextClient;
			free(removeNode->c);
			free(removeNode);
			return;
		}
		temp = temp->nextClient;
	}
	
}

void updatePos(int id, int x, int y){
	struct node *temp = clients;
	while(temp != NULL){
		if((temp->c->clientId) == id){
			temp->c->xPos = x;
			temp->c->yPos = y;
		}
		temp = temp->nextClient;
	}
}

struct node *getClients(){
	return clients;
}

