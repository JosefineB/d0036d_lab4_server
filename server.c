#include "server.h"



int isValidPosition(int id, int x, int y){
	struct node *temp = getClients();
	while(temp != NULL){
		if(temp->c->clientId != id){
			double distance = sqrt(((x - temp->c->xPos)*(x - temp->c->xPos)) + ((y - temp->c->yPos)*(y - temp->c->yPos)));
			if(distance < 24){
				return 0;
			}
		}
		temp = temp->nextClient;
	}
	return 1; 
}

void broadcast(char *msg, struct node *clients){
	struct node *temp =  clients;
	while(temp != NULL){
		write(temp->c->socketDesc, msg, strlen(msg)+1);
		temp = temp->nextClient; 
	}
}

void *handleClient(void *arg){
	struct node *client = (struct node*) arg;
	char input[100];
	char output[100];
	char delim[] = ":";
	char splitInput[6][6];
	while(1){
        	bzero( input, 100);
        	read(client->c->socketDesc,input,100);
		int i = 0;
		char *ptr = strtok(input, delim);
		while(ptr != NULL){
			strcpy(splitInput[i], ptr);
			i++;
			ptr = strtok(NULL, delim);
		}
		if(strcmp(ptr[0], "6")){ //move msg
			if(isValidPosition(client->c->clientId, atoi(ptr[3]), atoi(ptr[4]))){
				bzero(output,100);
				strcpy(output,"6:");
				strcat(output, client->c->clientId);
				strcat(output, ":0:");
				strcat(output, ptr[3]);
				strcat(output, ptr[4]);
				strcat(output, ":NEWPLAYERPOSITION");
				broadcast(&output, client);
			}
		}
		else if (strcmp(ptr[3], "JOIN")){
				bzero(output,100);
				strcpy(output,"4:");
				strcat(output, client->c->clientId);
				strcat(output, ":0:");
				strcat(output, ":JOIN");
				write(client->c->socketDesc, &output, strlen(output)+1);
				bzero(output, 100);
				strcpy(output,"4:");
				strcat(output, client->c->clientId);
				strcat(output, ":0:");
				strcat(output, ":NEWPLAYER");
				broadcast(&output, client);
		}
		else{
			//leave msg
			bzero(output, 100);
			strcpy(output,"4:");
			strcat(output, client->c->clientId);
			strcat(output, ":0:");
			strcat(output, ":PLAYERLEAVE");
			broadcast(&output, client);
			//close client socket
			close(client->c->socketDesc);
			removeClient(client->c->clientId);
			pthread_exit(NULL);
		}
    }
	
}

int main(){
    	int listen_fd, comm_fd;
	int clientCount = 0;
	struct node* currentClient;
 
    struct sockaddr_in servaddr;
 
    listen_fd = socket(AF_INET, SOCK_STREAM, 0);
 
    bzero( &servaddr, sizeof(servaddr));
 
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htons(INADDR_ANY);
    servaddr.sin_port = htons(22000);
 
    bind(listen_fd, (struct sockaddr *) &servaddr, sizeof(servaddr));
 
    listen(listen_fd, 10);
	
	printf("Listening for incomming clients on ip address: ");
	printf("%d\n", servaddr);
	
    while((comm_fd = accept(listen_fd, (struct sockaddr*) NULL, NULL)) != -1){
		clientCount++;
		insertClient(clientCount, comm_fd);
		currentClient = getClients();
		pthread_t clientThread;  
		pthread_create(&clientThread, NULL, handleClient,(void*)&currentClient);
	}
}
